import matplotlib.pyplot as plt
from matplotlib import gridspec
from graphics.attributes_histogram_axes_limits import attributes_axis_limits

import pandas as pd
import scipy.stats

from MyModel.config_text import *


def dashboard(animals, food, food_created, d, model='my_model'):
    cols, rows = 3, 3
    fig = plt.figure(figsize=(16, 9))
    gs = gridspec.GridSpec(ncols=cols, nrows=rows, figure=fig, hspace=0.4, wspace=0.2)
    axes = [None]*20
    axes[0] = fig.add_subplot(gs[0:2, 0:2], polar=True)
    polar_histogram(animals, ax=axes[0])
    axes[1] = fig.add_subplot(gs[2, 0])
    attributes_histogram(animals=animals, attr='phase', x_text=0.5, y_text=9.8, ax=axes[1])
    if model == 'my_model':
        axes[1].text(-np.pi -2.5,
                     12,
                     configuration_text_box(animals=animals),
                     bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.8),
                     va='bottom',
                     ha='left',
                     fontsize=8)
    axes[2] = fig.add_subplot(gs[2, 1])
    attributes_histogram(animals=animals, attr='loc', x_text=0., y_text=0, ax=axes[2])
    axes[4] = fig.add_subplot(gs[0, 2])
    newborn_phase_histogram(animals=animals, age_cutoff=1, x_text=1, y_text=9.5, ax=axes[4])
    # action_profile(animals, ax=axes[4])
    # attributes_histogram(animals=animals, attr='food_eaten', x_text=0, y_text=0, ax=axes[4])
    axes[5] = fig.add_subplot(gs[1, 2])
    attributes_histogram(animals=animals, attr='energy', x_text=0, y_text=0, ax=axes[5])
    axes[6] = fig.add_subplot(gs[2, 2])
    if FOOD_COMPETITION:
        food_availability_profile(food=food, food_created=food_created, ax=axes[6])
    else:
        attributes_histogram(animals=animals, attr='age', x_text=0, y_text=0, ax=axes[6])

    plt.suptitle(f'Inter-species and intra-species interactions shape biological rhythms '
                 f'and promote temporal niche differentiation?')
    plt.text(x=0.1, y=0.95, s=f'day: {d}', transform=fig.transFigure, ha='center')


def color_picker(i):
    if i == 'A':
        return 'teal'
    elif i == 'B':
        return 'orange'


def polar_histogram(animals, ax=None):
    if ax is None:
        ax = plt.gca()
    n = 120
    bins = np.linspace(0, 2*np.pi, n)
    data = {'sd': {}, 'pop': {}, 'mean': {}}
    hist_max = {}
    for obj in SPECIES_ALL:
        phases = np.array([a.phase+PROFILE_STATIC_PHASE[obj] for a in animals if a.species is obj])
        phases = np.where(phases >= 0, phases, phases + 2*np.pi)
        hist = np.histogram(phases, bins=bins)
        _bars = ax.bar(hist[1][:-1], hist[0], width=(2*np.pi/(n*1.1)), alpha=0.5, label=obj, color=color_picker(obj))
        # shifted_phases_std = np.where(phases > np.pi, phases - 2*np.pi, phases)
        # data['sd'][obj] = np.std(shifted_phases_std)
        # data['mean'][obj] = np.mean(shifted_phases_std)
        # data['pop'][obj] = len(phases)
        hist_max[obj] = max(hist[0])
    ax.set_ylim(0, 5 * (max(hist_max.values())//5 + 1))
    ax.set_title('polar histogram of self phase + static phase, in the last day')
    ax.legend()


def action_profile(animals, ax=None):
    if ax is None:
        ax = plt.gca()
    stats = {'daily_moves': {}}
    alpha = 0.5
    for obj in ['A', 'B']:
        # accumulated_moves = []
        # for x in animals:
        #     if x.species is obj:
        #         accumulated_moves.extend(x.nMovesInDay)
        # data = pd.DataFrame(data=np.vstack(accumulated_moves), columns=['time', 'moves'])
        # mean_data = data.groupby(by=['time']).mean()
        # stats['daily_moves'][obj] = sum(mean_data['moves'])
        # ax.scatter(x=24*mean_data.index/(2 * np.pi)+12, y=mean_data['moves'],
        #            label=obj, alpha=alpha, color=color_picker(obj))
        x = TIMESTEPS
        # y = action_profile_shape(x, PROFILE_STATIC_PHASE[obj])
        y = ACT_PROFILE_SHAPE[obj]
        ax.plot(x, y, alpha=alpha, ls=':', label=f'theory {obj}')
    # penalties = {'A': np.mean([i.phase_penalty for i in animals if i.species is 'A']),
    #              'B': np.mean([i.phase_penalty for i in animals if i.species is 'B'])}
    ax.legend(loc='upper right')
    ax.grid(ls=':')
    ax.set_title('activity profile, last day')
    ax.set_xlim(-3.5, 3.5)
    ax.set_ylim(-0.05, 0.35)
    ax.set_xlabel('24 hours in radians $[-\pi+\epsilon, \pi-\epsilon ]$ representation')
    ax.set_ylabel('activity (profile sum=1)')
    # ax.text(-17,
    #         7.6,
    #         f'$\\sum_{{day}}M_{{A}}$: {stats["daily_moves"]["A"]:.2f}\n'
    #         f'$\\sum_{{day}}M_{{B}}$: {stats["daily_moves"]["B"]:.2f}\n'
    #         f'$M(\\alpha)$={act_profile_txt}\n'
    #         f'$A_{{penalty}} = {penalties["A"]:.3f}, B_{{penalty}} = {penalties["B"]:.3f}$',
    #         bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.8),
    #         ha='left',
    #         va='top',
    #         fontsize=8
    #         )


def attributes_histogram(animals, attr, x_text=0.0, y_text=0.0, font=8, ax=None):
    if ax is None:
        ax = plt.gca()
    data = {attr: {}, 'sd': {}, 'mean': {}}
    bins = 50
    exclude = attr in ['age', 'food_eaten', 'energy', 'loc']
    for obj in SPECIES_ALL:
        data[attr][obj] = np.array([getattr(i, attr) for i in animals if i.species is obj])\
            if attr == 'phase' else np.array([getattr(i, attr) for i in animals if i.species is obj])
        _n = ax.hist(data[attr][obj], bins=bins, alpha=0.7, label=obj, color=color_picker(obj))
        if not exclude:
            mu, sigma = scipy.stats.norm.fit(data[attr][obj])
            x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
            y = scipy.stats.norm.pdf(x, mu, sigma)
            _d = ax.plot(x, y, color=color_picker(obj))
            data['sd'][obj] = sigma
            data['mean'][obj] = mu
    attributes_axis_limits[attr](ax)
    if not exclude:
        phase_distance = abs(data["mean"]["A"] + PROFILE_STATIC_PHASE["A"] - \
                         (data["mean"]["B"] + PROFILE_STATIC_PHASE["B"]))
        ax.text(x_text,
                y_text,
                f'$\\mu_A: {data["mean"]["A"]:.3f}, \\sigma_A: {data["sd"]["A"]:5.3f}$\n'
                f'$\\mu_B: {data["mean"]["B"]:.3f}, \\sigma_B: {data["sd"]["B"]:5.3f}$\n'
                f'hours gap: {phase_distance/(2*np.pi) * 24:5.3f}',
                bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5),
                va='top',
                ha='left',
                fontsize=font)
    ax.set_ylabel('count')
    # ax.set_title(f'{attr} histogram, last day')
    ax.legend(loc='upper left')
    ax.grid(ls=':')


def food_availability_profile(food, food_created, ax=None):
    if ax is None:
        ax = plt.gca()
    available = pd.DataFrame(data=np.vstack(food), columns=['time', 'food'])
    created = pd.DataFrame(data=np.vstack(food_created), columns=['time', 'food'])
    ax.scatter(available.time, available.food, label='available', alpha=0.5)
    ax.scatter(created.time, created.food, label='created', alpha=0.5)
    ax.set_title('hourly food, last day')
    ax.set_xlabel('24 hours in radians $[-\pi+\epsilon, \pi-\epsilon]$ representation')
    ax.set_ylabel('food available')
    ax.grid(ls=':')
    ax.legend(loc='upper right')
    ax.ticklabel_format(axis='y', style='sci', scilimits=[-3, 3])
    y_lim = 50*((max(max(available.food), max(created.food)))//50 + 1)
    ax.set_ylim(0, y_lim)


def newborn_phase_histogram(animals, age_cutoff, x_text=0.0, y_text=0.0, font=8, ax=None):
    if ax is None:
        ax = plt.gca()
    attr = 'newborn'
    data = {attr: {}, 'sd': {}, 'mean': {}}
    bins = 50
    for obj in SPECIES_ALL:
        data[attr][obj] = np.array([i.phase_penalty for i in animals if i.species is obj])
        _n = ax.hist(data[attr][obj], bins=bins, alpha=0.7, label=obj, color=color_picker(obj))
        mu, sigma = scipy.stats.norm.fit(data[attr][obj])
        x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
        y = scipy.stats.norm.pdf(x, mu, sigma)
        # _d = ax.plot(x, y, color=color_picker(obj))
        data['sd'][obj] = sigma
        data['mean'][obj] = mu
    # attributes_axis_limits[attr](ax)
    phase_distance = abs(data["mean"]["A"] + PROFILE_STATIC_PHASE["A"] - \
                     (data["mean"]["B"] + PROFILE_STATIC_PHASE["B"]))
    ax.text(x_text,
            y_text,
            f'$\\mu_A: {data["mean"]["A"]:.3f}, \\sigma_A: {data["sd"]["A"]:5.3f}$\n'
            f'$\\mu_B: {data["mean"]["B"]:.3f}, \\sigma_B: {data["sd"]["B"]:5.3f}$\n'
            f'hours gap: {phase_distance/(2*np.pi) * 24:5.3f}',
            bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5),
            va='top',
            ha='left',
            fontsize=font)
    ax.set_ylabel('count')
    # ax.set_title(f'{attr} histogram, last day')
    ax.set_xlim(0.4, 1.01)
    ax.set_ylim(0, 10)
    ax.legend(loc='upper left')
    ax.grid(ls=':')

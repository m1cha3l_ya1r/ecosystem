from config import *
from collections import Counter
import numpy as np


def dict_to_string(d, value_type='float'):
    if value_type == 'float':
        return ''.join([f'{i}: {d[i]:.3f}   ' for i in d.keys()])
    elif value_type == 'int':
        return ''.join([f'{i}: {d[i]}   ' for i in d.keys()])


def configuration_text_box(animals):
    species_count = Counter()
    for animal in animals:
        species_count[animal.species] += 1
    #
    return \
        f'$\\bf{{Model\ Type:}}$\n' \
        f'LIMITED_FOOD_RESOURCES: {FOOD_COMPETITION}\n' \
        f'FILTER_PHASES_BY_DISTANCE: {FILTER_PHASES_BY_DISTANCE}\n' \
        f'SAME_PHEROMONE = {REPRODUCTIVE_INTERFERENCE}\n' \
        f'' \
        f'$\\bf{{Environment:}}$\n' \
        f'N_DAYS = {N_DAYS}\n' \
        f'N_TRIALS = {N_TRIALS}\n' \
        f'ARENA_SIZE = {ARENA_SIZE}\n' \
        f'SPECIES_ALL = {SPECIES_ALL}\n' \
        f'N_TIMESTEPS = {N_TIMESTEPS}\n' \
        f'' \
        f'$\\bf{{Init\ Ecosystem:}}$\n' \
        f'AMP_INIT = {AMP_INIT}\n' \
        f'PHASE_INIT = {dict_to_string(PHASE_INIT)}\n' \
        f'N_ANIMALS_INIT = {dict_to_string(N_ANIMALS_INIT)}\n' \
        f'ENERGY_INIT = {ENERGY_INIT}\n' \
        f'$\\bf{{Population\ Characteristics:}}$\n' \
        f'PHASE_SD = {NEWBORN_PHASE_SD:.3f}\n' \
        f'POP_RATIOS = {dict_to_string(POP_RATIOS)}\n' \
        f'LIFESPAN = {LIFESPAN}\n' \
        f'SENSING_PRESENCE_DISTANCE = {SENSING_PRESENCE_DISTANCE}\n' \
        f'$P_{{SUCCESSFUL\ NEWBORN}}$ = {dict_to_string(P_NEWBORN)}\n' \
        f'MIN_ENERGY_FOR_MATING = {MIN_ENERGY_FOR_MATING:.3f}\n' \
        f'MOVES_PER_DAY = {MOVES_PER_DAY}\n' \
        f'MAX_ANIMALS = {MAX_ANIMALS}\n' \
        f'ENERGY_PER_MOVE = {ENERGY_PER_MOVE:.3f}\n' \
        f'ACT_PROFILE_SHAPE = {act_profile_txt}\n' \
        f'SHAPE_SUM = {dict_to_string(SHAPE_SUM)}\n' \
        f'PROFILE_STATIC_PHASE = {dict_to_string(PROFILE_STATIC_PHASE)}\n'\
        f'\n' \
        f'Population = {dict_to_string(species_count, value_type="int")}\n' \
        f'population: $R_{{init}}$: {N_ANIMALS_INIT["A"]/(N_ANIMALS_INIT["A"]+N_ANIMALS_INIT["B"]):.2f}, ' \
        f'$R_{{current}}$: {species_count["A"]/(species_count["A"]+species_count["B"]):.2f}'

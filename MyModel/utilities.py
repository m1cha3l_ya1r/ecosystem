import sqlite3

from numpy import sqrt, argmax, nan
from numpy.random import randint, shuffle, normal, choice, uniform
from numpy.random import random as np_random
from animal_class import Animal
from config import *
from collections import Counter
import sys


def start_population():
    # animals = []
    s = []
    for key in SPECIES_ALL:
        s = s + [key] * N_ANIMALS_INIT[key]
    shuffle(s)
    animals = [
        Animal(species=n,
               phase=PHASE_INIT[n] + normal(0.0, 2. * NEWBORN_PHASE_SD),
               energy=uniform(0, ENERGY_INIT),
               age=uniform(0.0, LIFESPAN)) for n in s
    ]
    shuffle(animals)
    return animals


def update_movable_animals_list(animals):
    return [i for i in animals if i.moves_per_step > 0 and not i.dead]


# food production
@njit()
def create_food_per_step(factor, size):
    return np_random(size) * factor


# create indexes vector
@njit()
def food_indexes_array(food_location):
    return [i for i, val in enumerate(food_location) if val > 0]


# create set of location from objects list
def dictionary_of_animals_count_in_location(animals):
    animals_count_in_location = Counter()
    for animal in animals:
        if not animal.dead:
            animals_count_in_location[animal.loc] += 1
    return animals_count_in_location


def animals_eating_by_food_location(time_step, animals, food_locations):
    # in the case MOVING_IS_EATING set to True, 'animals' passed as 'movable_animals' which might be an empty list
    if not animals:
        return food_locations
    # create a dictionary which its keys are the location and values are the number of animals in that location
    animals_count_in_location = dictionary_of_animals_count_in_location(animals)
    # create a dictionary of the amount to be eaten by the location
    to_be_eaten_in_location = {}
    for loc, count in animals_count_in_location.items():
        to_be_eaten = 1 if count <= food_locations[loc] else food_locations[loc] / count
        to_be_eaten_in_location[loc] = to_be_eaten
        food_locations[loc] -= (to_be_eaten * count)
    # iterate over to the population and eat according the location
    for animal in animals:
        if not animal.dead:
            food = to_be_eaten_in_location[animal.loc]
            animal.eat(time_step, food, PROFILE_STATIC_PHASE[animal.species])
    return food_locations


def clear_the_dead(animals):
    return [animal for animal in animals if not animal.dead]


def get_phase_distance_within_pair(pair):
    inertial_phases = pair[0].phase + PROFILE_STATIC_PHASE[pair[0].species], \
                      pair[1].phase + PROFILE_STATIC_PHASE[pair[1].species]
    phases_delta = abs(inertial_phases[0] - inertial_phases[1])
    if phases_delta > pi:
        phases_delta = 2 * pi - phases_delta
    phases_distance = phases_delta if phases_delta < pi else 2 * pi - phases_delta
    return phases_distance


def pick_a_mate(i, animals, candidates):
    """
    :param i:  index of the current candidate (integer)
    :param animals: animals list in community (object)
    :param candidates: list of other candidates
    :return: a pair for i if found, else None
    """
    for j in candidates:
        if not REPRODUCTIVE_INTERFERENCE and animals[i].species!=animals[j].species:
            continue
        else:
            if not animals[j].dead and \
                abs(animals[i].loc - animals[j].loc) <= SENSING_PRESENCE_DISTANCE and \
                    get_phase_distance_within_pair((animals[i], animals[j])) <= PHASE_SYNC_RANGE:
                return j
    return None


def random_pairs(animals):
    animals_count = len(animals)
    # create list of pairs
    selected_pairs = []
    # create list of candidates:
    candidates = list(range(animals_count))
    # create list of chosen:
    chosen_list = []
    for i in range(animals_count-1):
        if i not in chosen_list \
                and not animals[i].dead \
                and animals[i].energy >= MIN_ENERGY_FOR_MATING:
            potential_candidate = [j for j in candidates if i != j]
            shuffle(potential_candidate)
            ith_possible_pair = pick_a_mate(i, animals, potential_candidate)
            if ith_possible_pair:
                selected_pair = (i, ith_possible_pair)
                selected_pairs.append(selected_pair)
                chosen_list.append(ith_possible_pair)
                candidates.remove(ith_possible_pair)
                chosen_list.append(i)
                candidates.remove(i)
    # print(f'{time_step:.3f}: {len(selected_pairs)}, {len(animals)}')
    return selected_pairs


def dispersal(animals, steps_count, phase_range, limit_in_range):
    ranges = []
    animals_count_in_range = []
    range_vector = linspace(-pi, pi - phase_range, steps_count)
    for step in range_vector:
        ranges.append((step, step + phase_range))
        animals_count_in_range.append(len([a for a in animals if step <=
                                           a.phase + PROFILE_STATIC_PHASE[a.species]
                                           <= step + phase_range]))
        if step + phase_range > pi:
            break
    if max(animals_count_in_range) >= limit_in_range:
        high_density_index = argmax(animals_count_in_range)
        high_density_range = ranges[high_density_index]
        high_density_animals_list = [a for a in animals if high_density_range[0] <=
                                     a.phase + PROFILE_STATIC_PHASE[a.species]
                                     <= high_density_range[1]]
        inside_range_diluted = choice(
            a=high_density_animals_list,
            size=limit_in_range,
            replace=False)
        outside_range = [a for a in animals if high_density_range[0] > a.phase or a.phase > high_density_range[1]]
        return list(inside_range_diluted) + outside_range
    else:
        return animals


def engagement(animals, time_step, selected_pairs, p_newborn_dict):
    newborns = []
    pairs = selected_pairs
    for p in pairs:
        pair = (animals[p[0]], animals[p[1]])
        # convert self and static phases to be the object phase as in current ecosystem time
        inertial_phase = pair[0].phase + PROFILE_STATIC_PHASE[pair[0].species], \
                         pair[1].phase + PROFILE_STATIC_PHASE[pair[1].species]
        if FILTER_PHASES_BY_DISTANCE:
            phases_distance = get_phase_distance_within_pair(pair)
            # lower probability as the phase distance increase:
            phases_filter_by_distance = 1 - (phases_distance / pi) ** 2
        else:
            phases_filter_by_distance = 1
        # activity level in the specific time step
        p_action = sqrt(action_profile_shape(time_step + inertial_phase[0]) *
                        action_profile_shape(time_step + inertial_phase[1]))
        # total probability
        p_mating = p_action * phases_filter_by_distance
        is_mating = p_mating >= np_random()
        if is_mating:
            if pair[0].species == pair[1].species:
                # adding probabilities explicitly, although can be removed for performance
                p_newborn = p_newborn_dict[pair[0].species] > np_random()
                if p_newborn:
                    child = pair[0].sex(pair[1])
                    newborns = newborns + [child]
                    for p in pair:
                        p.fitness += 1
                        p.energy = p.energy * (1 - ENERGY_TO_CHILD)
                        p.dead = True if p.energy <= 1e-5 else False
                else:
                    for p in pair:
                        p.extinction += 1
                        p.energy = p.energy * (1 - ENERGY_TO_CHILD)
                        p.dead = True if p.energy <= 1e-5 else False
            else:
                for p in pair:
                    p.extinction += 1
                    p.energy = p.energy * (1 - 0.5 * ENERGY_TO_CHILD)
                    p.dead = True if p.energy <= 1e-5 else False
    return newborns


def obj_to_tuple(trail, day, time_step, obj, attributes=None):
    if attributes is None:
        attributes = ['species',
                      'loc',
                      'phase',
                      'phase_penalty',
                      'energy',
                      'food_eaten',
                      'age',
                      'moves_per_step',
                      'dead',
                      'fitness',
                      'extinction']
        vals = [getattr(obj, att) for att in attributes]
        # phase correction to inertial phase
        vals[2] = obj.phase + PROFILE_STATIC_PHASE[obj.species]
    else:
        return None
    return tuple([*[trail, day, time_step], *vals])


def save_to_db(cur, con, animals, trial, d, t, food_created_increase, food_location):
    # animals, trial, d, t, food_created_increase, food_location
    to_executemany_animals = []
    to_executemany_food = []
    for animal in animals:
        to_executemany_animals.append(obj_to_tuple(trial, d, t, animal))
    try:
        cur.executemany(f'insert into animals (trial, day, time, species, loc, phase, phase_penalty, '
                        f'energy, food_eaten, age, moves_per_step, dead, fitness, extinction) values'
                        f'({"?"+", ?"*13})', to_executemany_animals)
        cur.execute(f'insert into food (trial, day, time, food_created, food_available) values'
                    f'({trial}, {d}, {t}, {sum(food_created_increase)}, {sum(food_location)})')
    except sqlite3.Error as error:
        print(f'SQLite error: {" ".join(error.args)}')
        print(f'Exception class is: {error.__class__}')
    con.commit()

import sqlite3
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from matplotlib import gridspec
from scipy.stats import ttest_ind

palette_extinct = {'A extinct': (0.,0.5,1,1.), 'B extinct': (1,0.4,0.0,1.), 'both survived': (0,0.6,0.2,0.5), 'both extinct': (1,0.1,0.1,0.5)}
palette = {'A extinct': (0.,0.5,1,1.), 'B extinct': (1,0.4,0.0,1.), 'both survived': (0,0.6,0.2,0.5), 'both extinct': (1,0.1,0.1,0.5)}
def get_last_day_per_trial(db):
    get_max_day_per_trial = \
        '''
        SELECT t1.* 
        FROM animals t1 
        INNER JOIN (SELECT trial, MAX(day) max_day 
                FROM animals 
                GROUP BY trial) t2 
        ON t1.trial=t2.trial AND t1.day=t2.max_day
        INNER JOIN (SELECT trial, day, MAX(time) max_time
                    FROM animals
                    GROUP BY trial, day) t3
        ON t1.trial=t3.trial AND t1.day=t3.day AND t1.time=t3.max_time
        '''
    con_comp = sqlite3.connect(db)
    # cur_comp = con_comp.cursor()
    return pd.read_sql_query(get_max_day_per_trial, con_comp)


def get_mean_phase_age_count_per_species_in_time(db):
    query = \
        '''
        SELECT trial, day, time, species, AVG(phase) as mean_phase, 
        (AVG(phase*phase) - AVG(phase)*AVG(phase)) as variance, COUNT(species) as count,
        AVG(age) mean_age, SUM(CASE WHEN age=0 THEN 1 ELSE 0 END) as newborn
        FROM animals
        GROUP BY trial, day, time, species
        '''
    con_inter = sqlite3.connect(db)
    try:
        return pd.read_sql_query(query, con_inter)
    except ValueError as e:
        print(e)


def get_food_eaten(db):
    query = \
        '''
        SELECT day,
        SUM(food_eaten) as food_eaten_daily,
        AVG(food_eaten) as mean_food_eaten, 
        (AVG(food_eaten*food_eaten) - AVG(food_eaten)*AVG(food_eaten)) as var_food_eaten,
        AVG(energy) as mean_energy, 
        (AVG(energy*energy) - AVG(energy)*AVG(energy)) as var_energy 
        FROM animals
        GROUP BY day
        '''
    con_inter = sqlite3.connect(db)
    try:
        return pd.read_sql_query(query, con_inter)
    except ValueError as e:
        print(e)


def last_day_query_features_preps(df):
    df = df[df.dead == 0]
    df = df.groupby(['trial', 'day', 'species']).size().unstack(fill_value=0).reset_index()
    # separate trial both survived to the end from ended from extinction of at least one species
    df['A_died'] = df.A<2
    df['B_died'] = df.B<2
    df['extinct'] = df.apply(deaths, axis=1)
    df['extinct'].replace({'none': 'survived'}, inplace=True)
    return df


def deaths(x):
    if x.A_died and x.B_died:
        return 'both extinct'
    elif x.A_died and not x.B_died:
        return 'A extinct'
    elif not x.A_died and x.B_died:
        return 'B extinct'
    elif not x.A_died and not x.B_died:
        return 'both survived'


def color_picker(i):
    if i == 'A':
        return 'magenta'
    elif i == 'B':
        return 'cyan'


def fig_3_competition_ri_distributions(competition_df, ri_df, palette, community_size, y_lim):
    titles = ['Fig. 3A: Resource competition distribution', 'Fig. 3B: Reproduction interference distribution']
    data = [competition_df, ri_df]
    cols, rows = 2, 1
    fig = plt.figure(figsize=(12, 5))
    gs = gridspec.GridSpec(ncols=cols, nrows=rows, figure=fig, hspace=0.4, wspace=0.2)
    axes = [None]*2
    axes[0] = fig.add_subplot(gs[0, 0])
    axes[1] = fig.add_subplot(gs[0, 1])
    for i, d in enumerate(data):
        sns.histplot(data=d,
                     x=d['day'],
                     hue='extinct',
                     hue_order=['A extinct', 'B extinct', 'both survived', 'both extinct'],
                     multiple='dodge',
                     ax=axes[i],
                     log_scale=True,
                     palette=palette,
                     bins=20)
        axes[i].set_title(titles[i], fontsize=14)
        axes[i].set_xlim(10, 2.5 * 1e3)
        axes[i].set_ylim(0, y_lim)
        axes[i].set_xlabel('Trial last day', fontsize=12)
        axes[i].set_ylabel('Number of trials', fontsize=12)
        axes[i].tick_params(labelsize=12)
        # axes[i].grid(':')
        axes[i].get_legend().set_title(None)
    fig.text(0.1, 0.02, f'$N_0$ = {community_size}', bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
    plt.savefig('Fig_3_competition_interference_hist_died_hue.png', dpi=300, bbox_inches='tight')


def create_mean_delta_phases_df(raw_df, mode):
    mean_delta_phase = {'trial': [], 'A_mean': [], 'A_std': [], 'B_mean': [], 'B_std': [], 'p_value': [], 'stats': []}
    trial = raw_df.query('day==2000').trial.unique()
    for t in trial:
        mean_delta_phase['trial'] = mean_delta_phase['trial'] + [t]
        stats, pvalue = ttest_ind(raw_df.query(f'species=="A" & trial=={t}').phase,
                                  raw_df.query(f'species=="B" & trial=={t}').phase)
        mean_delta_phase['p_value'] = mean_delta_phase['p_value'] + [pvalue]
        mean_delta_phase['stats'] = mean_delta_phase['stats'] + [stats]
        for s in ['A', 'B']:
            mean_delta_phase[f'{s}_mean'] = mean_delta_phase[f'{s}_mean'] + [
                np.mean(raw_df.query(f'species=="{s}" & trial=={t}').phase)]
            mean_delta_phase[f'{s}_std'] = mean_delta_phase[f'{s}_std'] + [
                np.std(raw_df.query(f'species=="{s}" & trial=={t}').phase)]
    mean_delta_phase = pd.DataFrame.from_dict(mean_delta_phase)
    mean_delta_phase['delta'] = np.abs(np.array(mean_delta_phase['A_mean']) - np.array(mean_delta_phase['B_mean']))
    mean_delta_phase['delta'].where(mean_delta_phase['delta'] < np.pi,
                                    2 * np.pi - mean_delta_phase['delta'],
                                    inplace=True)
    mean_delta_phase['mode'] = mode

    return mean_delta_phase


def mean_phase(df, mode, query='day!=2000'):
    mean_phase_dict = {'trial': [], 'A_mean': [], 'A_std': [], 'B_mean': [], 'B_std': []}
    trial = df.query(query).trial.unique()
    for t in trial:
        mean_phase_dict['trial'] = mean_phase_dict['trial'] + [t]
        for s in ['A', 'B']:
            mean_phase_dict[f'{s}_mean'] = mean_phase_dict[f'{s}_mean'] + \
                                           [np.mean(df.query(f'species=="{s}" & trial=={t}').phase)]
            mean_phase_dict[f'{s}_std'] = mean_phase_dict[f'{s}_std'] + \
                                          [np.std(df.query(f'species=="{s}" & trial=={t}').phase)]
    mean_phase_df = pd.DataFrame.from_dict(mean_phase_dict)
    mean_phase_df['mode'] = mode
    return mean_phase_df


def fig_4_mean_delta_phase_survived(competition_survived_df, ri_survived_df, community_size_t0, legend):
    mean_delta_phase_both = pd.concat([competition_survived_df, ri_survived_df])
    fig = plt.figure(figsize=(6, 5))
    ax = plt.subplot(111)
    h = sns.histplot(data=mean_delta_phase_both.melt(id_vars=['trial', 'mode'], value_vars=['delta']),
                     x='value',
                     hue='mode',
                     hue_order=['competition', 'RI'],
                     multiple='dodge',
                     bins=20,
                     axes=ax,
                     legend=True)
    plt.title(
        'Fig. 4: Distribution of $\\left| \\bar{\\alpha_A} - \\bar{\\alpha_B} \\right|$ '
        'at the trials ended without extinction',
        fontsize=14)
    plt.xlim(0, 3 * np.pi / 2)
    plt.xticks(np.arange(0, 2 * np.pi, step=(np.pi / 2)), ['0', '$\\frac{π}{2}$', 'π', '$\\frac{3π}{2}$'], fontsize=12)
    plt.xlabel('Phase angle displacement between species [Rad]', fontsize=12)
    plt.ylabel('Number of trials', fontsize=12)
    plt.tick_params(labelsize=12)
    # plt.grid(':')
    plt.legend(legend)
    fig.text(0.02, 0.02, f'$N_0$ = {community_size_t0}', bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
    plt.savefig('Fig_4_mean_delta_phase_survived_trials.png', dpi=100, bbox_inches='tight')


def fig_5_polar_histogram_survive_vs_extinct(df_mean, df_delta_mean, community_size_t0, params, y_ticks):
    cols, rows = 2, 1
    fig = plt.figure(figsize=(12, 5))
    gs = gridspec.GridSpec(ncols=cols, nrows=rows, figure=fig, hspace=0.4, wspace=0.2)
    axes = [None] * 2

    axes[0] = fig.add_subplot(gs[0, 0], polar=True)
    axes[1] = fig.add_subplot(gs[0, 1], polar=True)

    n = 60
    bins = np.linspace(0, 2 * np.pi, n)
    hist_max = {}

    for obj in ['A_mean', 'B_mean']:
        phases_survived = np.array(df_delta_mean[obj])
        phases_survived = np.where(phases_survived >= 0, phases_survived, phases_survived + 2 * np.pi)
        hist_survived = np.histogram(phases_survived, bins=bins)
        _bars_survived = axes[0].bar(hist_survived[1][:-1], hist_survived[0], width=(2 * np.pi / (n * 1.1)), alpha=0.5,
                                     label=obj, color=color_picker(obj[0]))

        phases_extinct = np.array(df_mean[obj])
        phases_extinct = np.where(phases_extinct >= 0, phases_extinct, phases_extinct + 2 * np.pi)
        hist_extinct = np.histogram(phases_extinct, bins=bins)
        _bars = axes[1].bar(hist_extinct[1][:-1], hist_extinct[0], width=(2 * np.pi / (n * 1.1)), alpha=0.5, label=obj,
                            color=color_picker(obj[0]))

    xT = plt.xticks()[0]
    xL = ['0', '$\\frac{\pi}{4}$', '$\\frac{\pi}{2}$', '$\\frac{3}{4}\pi$', '$\pi\ |-\pi$', '-$\\frac{3}{4}\pi$',
          '-$\\frac{\pi}{2}$', '-$\\frac{\pi}{4}$']

    axes[0].set_title(
        params['survived_title'],
        y=1.1, fontsize=14)
    axes[1].set_title(
        params['extinct_title'],
        y=1.1, fontsize=14)

    axes[0].set_yticks(y_ticks[0])
    axes[1].set_yticks(y_ticks[1])

    for i in range(2):
        axes[i].set_xticklabels(xL)
        axes[i].tick_params(labelsize=12)
        axes[i].legend(['$\\bar{\\alpha_A}$', '$\\bar{\\alpha_B}$'], loc=6, fontsize=12)

    fig.text(0.15, 0.1, f'$N_0$ = {community_size_t0}', bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
    plt.savefig(params['save_to_file'], dpi=100,
                bbox_inches='tight')


def add_hours_end(df):
    hours = []
    end = []
    for t in df.trial.unique():
        temp_df = df[df.trial==t]
        length = len(temp_df)
        hours = hours + list(range(length))
        if length>=2001*24:
            end = end + ['survived']*length
        else:
            end = end + ['extinct']*length
    return hours, end


def df_over_time_preps(df, time_step):
    df["stdev"] = np.sqrt(df.variance)
    df_pivot = df.pivot_table(index=['trial', 'day', 'time'], columns=['species']).reset_index()
    hours, end = add_hours_end(df_pivot)
    df_pivot['hours'] = hours
    df_pivot['end'] = end
    df_pivot['population'] = df_pivot["count"].A + df_pivot["count"].B
    df_pivot['abs_delta_mean'] = abs(df_pivot.mean_phase.A - df_pivot.mean_phase.B)
    df_pivot.columns = [' '.join(col).strip() for col in df_pivot.columns.values]
    df_pivot['days_units'] = df_pivot['hours'] / time_step
    df_pivot.rename(columns={'count A': 'A', 'count B': 'B'}, inplace=True)
    return df_pivot


def plot_stdev_of_phase(df, y, title, ax=None):
    if ax is None:
        ax = plt.gca()
    for t in df.trial.unique():
        temp_df = df[df.trial==t]
        sns.lineplot(data=temp_df,
                     x='days_units',
                     y=y,
                     hue='end',
                     legend=False,
                     alpha=0.25,
                     palette={'survived': (0., 0.5, 1, 1.), 'extinct': (1, 0.4, 0.0, 1.)})
    plt.grid(':')
    plt.xlabel('days')
    plt.ylabel('$\\left| \\bar{\\alpha_A} - \\bar{\\alpha_B} \\right|$')
    leg = plt.legend(['survived', 'extinct'], loc='upper left')
    leg.legendHandles[0].set_color((0.,0.5,1,1.))
    leg.legendHandles[1].set_color((1,0.4,0.0,1.))
    plt.xlim(0, 200)
    plt.title(f'interference {title} develop over time, per trial')


def fig_6_mean_delta_over_time(df, params, community_size_t0, xlim):
    fig = plt.figure(figsize=(10, 5))
    for t in df.trial.unique():
        temp_df = df[df.trial == t]
        sns.lineplot(data=temp_df, x='days_units', y='abs_delta_mean', hue='end', legend=False, alpha=0.25,
                     palette={'survived': (0., 0.5, 1, 1.), 'extinct': (1, 0.4, 0.0, 1.), 'none': (0, 0.8, 0.2, 1)}
                     )
    # plt.grid(':')
    plt.xlabel('Days')
    plt.ylabel('Phase angle displacement over time $\\left| \\bar{\\alpha_A} - \\bar{\\alpha_B} \\right|$')
    leg = plt.legend(['survived', 'extinct'], loc='upper left')
    leg.legendHandles[0].set_color((0., 0.5, 1, 1.))
    leg.legendHandles[1].set_color((1, 0.4, 0.0, 1.))
    plt.xlim(xlim)
    plt.ylim(0, np.pi)
    plt.yticks(np.arange(0, np.pi + np.pi / 4, step=(np.pi / 4)),
               ['0', '$\\frac{π}{4}$', '$\\frac{π}{2}$', '$\\frac{3π}{4}$', 'π'])
    plt.title(params['title'])
    fig.text(0.05, 0.05, f'$N_0$ = {community_size_t0}', bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
    plt.savefig(params['save_to_file'], dpi=100, bbox_inches='tight')


def plot_population_in_time(df, y, mode, xlim, ylabel, fig, ax=None):
    if ax is None:
        ax = plt.gca()
    for t in df.trial.unique():
        temp_df = df[df.trial==t]
        sns.lineplot(data=temp_df[temp_df.days_units<=xlim[1]],
                     x='days_units',
                     y=y,
                     hue='end',
                     legend=False,
                     alpha=0.25,
                     palette={'survived': (0., 0.5, 1, 1.), 'extinct': (1, 0.4, 0.0, 1.)})
    # plt.grid(':')
    plt.xlabel('Days')
    plt.ylabel(ylabel)
    leg = plt.legend(['survived', 'extinct'], loc='upper left')
    leg.legendHandles[0].set_color((0.,0.5,1,1.))
    leg.legendHandles[1].set_color((1,0.4,0.0,1.))
    plt.xlim(xlim)
    plt.ylim(0, 330)
    plt.title(f'Fig. {fig}: {mode} {ylabel} develop over time, per trial')


def fig_7_8_population_in_time(df, mode, community_size_t0, fig_number):
    cols, rows = 1, 3
    fig = plt.figure(figsize=(9, 14))
    gs = gridspec.GridSpec(ncols=cols, nrows=rows, figure=fig, hspace=0.3, wspace=0.0)
    axes = [None] * 3
    ylabel = ['Community size', 'Population A size', 'Population B size']
    figname = [str(fig_number)+t for t in ['A', 'B', 'C']]
    for i, y in enumerate(['population', 'A', 'B']):
        axes[i] = fig.add_subplot(gs[i, 0])
        plot_population_in_time(df, y, mode, (0, 250), ylabel[i], figname[i],
                                ax=axes[i])
    # fig.text(0.05, 0.09, f'$N_0$ = {community_size_t0}', bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
    plt.savefig(f'Fig_{fig_number}_{mode}_pop_in_time.png', dpi=100, bbox_inches='tight')


def pop_ratio_log2days(df_in_time, df_last_day):
    df = pd.merge(df_in_time, df_last_day[['trial', 'extinct']], left_on='trial', right_on='trial', copy=False)
    df['ratio'] = np.log2(df_in_time.apply(lambda x: x.A / x.B if x.B > 0 else None, axis=1))
    df['log2_days'] = np.log2(df_in_time.days_units)
    return df


def fig_9_plot_population_ratio(df, y, xlim, figname, mode, ylabel, community_size_t0, extinct_palette, ax=None):
    fig = plt.figure(figsize=(10, 5))
    if ax is None:
        ax = plt.gca()
    for t in df.trial.unique():
        temp_df = df[df.trial == t]
        sns.lineplot(data=temp_df, x='days_units', y=y, hue='extinct', legend=False, alpha=0.25, palette=extinct_palette)
    # plt.grid(':')
    plt.xlabel('Days', fontsize=12)
    plt.ylabel(ylabel, fontsize=12)
    leg = plt.legend(['A extinct', 'B extinct', 'both survived', 'both extinct'], loc='upper right')
    for i, key in enumerate(extinct_palette.keys()):
        leg.legendHandles[i].set_color(extinct_palette[key])
    plt.xlim(xlim)
    plt.ylim(-8, 8)
    plt.title(f'Fig. {figname}: {mode} ratio of species population density {ylabel}', fontsize=14)
    ax.add_artist(leg)
    # fig.text(0.05, 0.05, f'$N_0$ = {community_size_t0}', bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5))
    plt.savefig(f'Fig_{figname}_{mode}_ratio_in_time.png', dpi=100, bbox_inches='tight')


def plot_birth_rate(df_ri_in_time, df_competition_in_time, ax=None):
    fig = plt.figure(figsize=(10, 5))
    df_dict = {
        'RI': df_ri_in_time,
        'Competition': df_competition_in_time
    }
    if ax is None:
        ax = plt.gca()

    for key in df_dict.keys():
        df_dict[key]['newborns'] = df_dict[key]['newborn A'] + df_dict[key]['newborn B']
        df_dict[key]['birth_rate'] = df_dict[key]['newborns'] / (df_dict[key]['population'] - df_dict[key]['newborns'])
        sns.scatterplot(data=df_dict[key][['day', 'newborns', 'birth_rate', 'extinct']]
                        .groupby(['day']).agg(np.mean).reset_index(),
                        x='day',
                        y='birth_rate',
                        s=10,
                        label=key,
                        ax=ax)
    ax.set_title('birth rate daily mean')
    # ax.set_ylim(0, 0.005)
    ax.set_xlim(0, 250)
    ax.set_ylabel('daily mean birth rate')
    ax.grid(':')
    plt.savefig('birth_rate.png', dpi=100, bbox_inches='tight')


def plot_mean_food_eaten_daily(food_eaten_df, y_max):
    fig = plt.figure(figsize=(10, 5))
    cols, rows = 1, 2
    fig = plt.figure(figsize=(10, 10))
    gs = gridspec.GridSpec(ncols=cols, nrows=rows, figure=fig, hspace=0.3, wspace=0.0)
    axes = [None] * 2
    ylabel = ['Mean food eaten daily', 'SD of food eaten daily']
    for i, param in enumerate(['mean_food_eaten', 'stdev_food_eaten']):
        axes[i] = fig.add_subplot(gs[i, 0])
        sns.lineplot(data=food_eaten_df, x='day', y=param,
                     alpha=0.7,
                     ax=axes[i])
        axes[i].set_ylim(0, y_max)
        axes[i].set_ylabel(ylabel[i])
        axes[i].set_xlabel('days')


def plot_energy_daily(food_eaten_df, y_max):
    fig = plt.figure(figsize=(10, 5))
    cols, rows = 1, 2
    fig = plt.figure(figsize=(10, 10))
    gs = gridspec.GridSpec(ncols=cols, nrows=rows, figure=fig, hspace=0.3, wspace=0.0)
    axes = [None] * 2
    ylabel = ['Mean energy daily', 'SD of energy daily']
    for i, param in enumerate(['mean_energy', 'stdev_energy']):
        axes[i] = fig.add_subplot(gs[i, 0])
        sns.lineplot(data=food_eaten_df, x='day', y=param,
                     alpha=0.7,
                     ax=axes[i])
        axes[i].set_ylim(0, y_max)
        axes[i].set_ylabel(ylabel[i])
        axes[i].set_xlabel('days')